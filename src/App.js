import React from "react";
import "./App.css";
import Navbar from "./components/navbar/Navbar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Blog from "./components/blog/Blog";
import Footer from "./components/footer/Footer";
import Home from "./components/home/Home";
import Contact from "./components/contact/Contact";
import TermsAndConditions from "./components/terms-and-conditions/TermsAndConditions";
import Photos from "./components/photos/Photos";
import PhotosContextProvider from "./contexts/PhotosContext";
import BlogContextProvider from "./contexts/BlogContext";
import UpdateForecastContextProvider from "./contexts/UpdateForecastContext";

function App() {
  return (
    <Router>
      <BlogContextProvider>
        <UpdateForecastContextProvider>
          <PhotosContextProvider>
            <div className="App">
              <Navbar />
              <Switch>
                <Route exact path="/">
                  <Home />
                </Route>
                <Route exact path="/blog">
                  <Blog />
                </Route>
                <Route exact path="/contact">
                  <Contact />
                </Route>
                <Route exact path="/terms">
                  <TermsAndConditions />
                </Route>
                <Route exact path="/photos">
                  <Photos />
                </Route>
              </Switch>
              <Footer />
            </div>
          </PhotosContextProvider>
        </UpdateForecastContextProvider>
      </BlogContextProvider>
    </Router>
  );
}

export default App;
