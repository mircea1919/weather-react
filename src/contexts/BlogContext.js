import React, { createContext, useState, useEffect } from "react";
export const BlogContext = createContext();
// This on have no api Key simple req to end point
const BlogContextProvider = (props) => {
  const [posts, setPosts] = useState([]);
  const getPosts = async () => {
    const URL = "https://jsonplaceholder.typicode.com/posts";
    const res = await fetch(URL, { method: "GET" });
    const data = await res.json();
    return data;
  };
  useEffect(() => {
    getPosts()
      .then((data) => {
        setPosts(data.splice(1, 10));
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <BlogContext.Provider value={{ posts }}>
      {props.children}
    </BlogContext.Provider>
  );
};

export default BlogContextProvider;
