import React, { createContext, useState, useEffect } from "react";
export const PhotosContext = createContext();

const PhotosContextProvider = (props) => {
  // Pexels Photos API KEY
  const apiKey = "563492ad6f91700001000001490c131a598d42a08418882d86ba77ba";
  const initialState = [];

  const [photos, setPhotos] = useState(initialState);
  const [query, setQuery] = useState("weather");
  const getImages = async (query) => {
    const url = `https://api.pexels.com/v1/search?query=${query}`;
    const res = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: apiKey,
      },
    });
    const data = await res.json();
    return data;
  };
  useEffect(() => {
    getImages(query)
      .then((data) => {
        setPhotos(data.photos);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <PhotosContext.Provider
      value={{ photos, setPhotos, getImages, query, setQuery }}
    >
      {props.children}
    </PhotosContext.Provider>
  );
};
export default PhotosContextProvider;
