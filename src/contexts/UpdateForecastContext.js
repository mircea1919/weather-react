import React, { createContext, useState, useEffect } from "react";
export const UpdateForecastContext = createContext();
const UpdateForecastContextProvider = (props) => {
  // ACCU Weather API KEY
  const apiKey = "FzGENmcAD7oG6RzaC0lTEXKj5ql83Qdu";
  const [forecast, setForecast] = useState();
  const [city, setCity] = useState("mianwali");
  const [nextFiveDays, setNextFiveDays] = useState();
  //   const [weather, setWeather] = useState();

  const getCity = async (city) => {
    const URL = `http://dataservice.accuweather.com/locations/v1/cities/search`;
    const query = `?apikey=${apiKey}&q=${city}`;
    const res = await fetch(URL + query);
    const data = await res.json();
    return data[0];
  };

  const getCityWeather = async (id) => {
    const URL = "http://dataservice.accuweather.com/currentconditions/v1/";
    const query = `${id}/?apikey=${apiKey}`;
    const res = await fetch(URL + query);
    const data = await res.json();
    return data[0];
  };
  const getFiveDaysWeather = async (id) => {
    const URL = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/";
    const query = `${id}?apikey=${apiKey}`;
    const res = await fetch(URL + query);
    const data = await res.json();
    return data;
  };
  useEffect(() => {
    getCity(city)
      .then((data) => {
        return getCityWeather(data.Key);
      })
      .then((data) => {
        setForecast(data);
      })
      .catch((err) => console.log(err));
  }, []);
  useEffect(() => {
    getCity(city)
      .then((data) => {
        console.log(data);
        return getFiveDaysWeather(data.Key);
      })
      .then((data) => {
        setNextFiveDays([...data.DailyForecasts]);
        console.log(nextFiveDays);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <UpdateForecastContext.Provider
      value={{
        forecast,
        setForecast,
        city,
        getCity,
        getCityWeather,
        setCity,
        nextFiveDays,
        getFiveDaysWeather,
        setNextFiveDays,
      }}
    >
      {props.children}
    </UpdateForecastContext.Provider>
  );
};

export default UpdateForecastContextProvider;
