import React from "react";

const Alert = ({ alertText, alertClass }) => {
  return (
    <div class={`alert ${alertClass}`} role="alert">
      {alertText}
    </div>
  );
};

export default Alert;
