import React from "react";
import { Link } from "react-router-dom";
const Breadcrumb = ({ pageName }) => {
  return (
    <div className="container">
      <div className="breadcrumb">
        <Link to="/">Home</Link>
        <span>{pageName}</span>
      </div>
    </div>
  );
};

export default Breadcrumb;
