import React, { Fragment, useContext } from "react";
import { UpdateForecastContext } from "../../contexts/UpdateForecastContext";
import CurrentWeather from "./CurrentWeather";

const Forecast = () => {
  const {
    city,
    setCity,
    getCity,
    getCityWeather,
    nextFiveDays,
    setNextFiveDays,
    getFiveDaysWeather,
    setForecast,
  } = useContext(UpdateForecastContext);
  const onChangeHandler = (e) => {
    setCity(e.target.value);
  };
  const onSubmitHandler = (e) => {
    e.preventDefault();
    getCity(city)
      .then((data) => {
        return getCityWeather(data.Key);
      })
      .then((data) => {
        setForecast(data);
      })
      .catch((err) => console.log(err));

    getCity(city)
      .then((data) => {
        return getFiveDaysWeather(data.Key);
      })
      .then((data) => {
        setNextFiveDays(data.DailyForecasts);
        console.log(nextFiveDays);
      })
      .catch((err) => console.log(err));
  };
  return (
    <Fragment>
      <div onSubmit={onSubmitHandler} className="row forecast">
        <div className="col-md-12">
          <form className="forecast-form">
            <div className="input-group">
              <input
                type="text"
                className="form-control"
                placeholder="Search Your desire location"
                onChange={onChangeHandler}
              />
              <div className="input-group-append">
                <input
                  type="submit"
                  className="input-group-text btn-forecast-search"
                  id="basic-addon2"
                  value="Find"
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    </Fragment>
  );
};
export default Forecast;
