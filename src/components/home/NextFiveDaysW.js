import React, { useContext } from "react";
import { UpdateForecastContext } from "../../contexts/UpdateForecastContext";
import Day from "./Day";
const NextFiveDaysW = () => {
  const { nextFiveDays } = useContext(UpdateForecastContext);
  console.log(nextFiveDays);
  if (nextFiveDays) {
    console.log(nextFiveDays);
    return nextFiveDays.map((nextDay) => {
      const now = new Date(nextDay.Date);
      const weekday = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
      ];
      const miniTempDegree = ((nextDay.Temperature.Minimum.Value - 32) * 5) / 9;
      const maxTempDegree = ((nextDay.Temperature.Maximum.Value - 32) * 5) / 9;

      return (
        <Day
          key={nextDay.EpochDate}
          toDay={weekday[now.getDay()]}
          icon={nextDay.Day.Icon}
          miniTemp={Math.round(miniTempDegree)}
          maxTemp={Math.round(maxTempDegree)}
        />
      );
    });
  } else return null;
};

export default NextFiveDaysW;
