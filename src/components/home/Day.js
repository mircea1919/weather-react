import React from "react";
const Day = ({ toDay, icon, miniTemp, maxTemp }) => {
  return (
    <div className="nextDay">
      <p>{toDay}</p>
      <div className="icon-div bg-blue d-inline-flex mr-3">
        <img
          className="mx-auto w-100"
          src={`https://myicons.netlify.com/icons/${icon}.svg`}
          alt=""
        />
      </div>
      <div className="avg">
        <span className="d-block">{miniTemp} &deg; C</span>
        <span className="d-block">{maxTemp} &deg; C</span>
      </div>
    </div>
  );
};

export default Day;
