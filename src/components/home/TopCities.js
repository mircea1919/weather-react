import React from "react";
const TopCities = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-4 pl-md-0 pl-3">
          <div className="my-3">
            <img
              className="w-100"
              src="https://images.pexels.com/photos/1162251/pexels-photo-1162251.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
              alt=""
            />
            <h4 className="text-light mt-3">Weather in Images</h4>
            <p className="text-justify">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Non,
              numquam? Officia quod velit dolores quia odit molestiae voluptas
              consequatur. Tenetur nihil saepe nostrum molestias ut a voluptatem
              eos eligendi non.
            </p>
            <a href="photos.html" className="btn text-white rounded-me bg-blue">
              Explore more
            </a>
          </div>
        </div>
        <div className="col-lg-8 my-2 pr-md-0 pr-3">
          <h1 className="col-12 mb-4 text-light">Top Cities</h1>
          <div className="d-flex my-3">
            <div className="col-lg-6 d-none d-sm-block card bg-dark py-3">
              <img
                className="w-100"
                src="https://assets.bwbx.io/images/users/iqjWHBFdfxIU/i_dAlMPaMwf4/v1/1200x-1.jpg"
                alt=""
              />
              <p className="text-justify mt-3">
                Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                Voluptas perferendis, optio necessitatibus.
              </p>
            </div>
            <div className="col-lg-6 d-lg-flex justify-content-lg-around">
              <ul className="cities pl-3">
                <li>
                  <h4>Countries</h4>
                </li>
                <li>
                  <a href="#f">
                    <i className="fa fa-angle-right mr-2"></i>
                    America
                  </a>
                </li>
                <li>
                  <a href="#f">
                    <i className="fa fa-angle-right mr-2"></i>
                    United State
                  </a>
                </li>
                <li>
                  <a href="#f">
                    <i className="fa fa-angle-right mr-2"></i>
                    United Kingdum
                  </a>
                </li>
                <li>
                  <a href="#f">
                    <i className="fa fa-angle-right mr-2"></i>
                    America
                  </a>
                </li>
              </ul>
              <ul className="cities pl-3">
                <li>
                  <h4>Cities</h4>
                </li>
                <li>
                  <a href="#f">
                    <i className="fa fa-angle-right mr-2"></i>
                    America
                  </a>
                </li>
                <li>
                  <a href="#f">
                    <i className="fa fa-angle-right mr-2"></i>
                    United State
                  </a>
                </li>
                <li>
                  <a href="#f">
                    <i className="fa fa-angle-right mr-2"></i>
                    United Kingdum
                  </a>
                </li>
                <li>
                  <a href="#f">
                    <i className="fa fa-angle-right mr-2"></i>
                    America
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TopCities;
