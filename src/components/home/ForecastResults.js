import React, { Fragment, useContext, useState, useEffect } from "react";
import { UpdateForecastContext } from "../../contexts/UpdateForecastContext";
import CurrentWeather from "./CurrentWeather";
import NextFiveDaysW from "./NextFiveDaysW";

const ForecastResults = () => {
  const { city } = useContext(UpdateForecastContext);
  return (
    <Fragment>
      <div className="container forecastUpdates">
        <div className="row py-lg-3">
          <div className="col-lg-4 my-4 d-flex justify-content-md-between">
            <div className="text-light">
              <CurrentWeather />
            </div>
            <div className="">
              <h3 className="text-primary font-weight-light">{city}</h3>
            </div>
          </div>
          <div className="col-lg-8 my-4 border-left">
            <h2 className="text-light mb-3">Next Five Days</h2>
            <div className="next-five-days">
              <NextFiveDaysW />
            </div>
          </div>
        </div>
      </div>
      <div className="container mb-5">
        <div className="row img-weather">
          <h1 className="text-center">Weather App is Live</h1>
        </div>
      </div>
    </Fragment>
  );
};

export default ForecastResults;
