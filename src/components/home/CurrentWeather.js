import React, { Fragment, useContext } from "react";
import { UpdateForecastContext } from "../../contexts/UpdateForecastContext";
import Icon from "../../img/icons/12.svg";

const CurrentWeather = () => {
  const { forecast } = useContext(UpdateForecastContext);

  if (forecast) {
    return (
      <Fragment>
        <h3 className="font-weight-light">{forecast.WeatherText}</h3>
        <span className="d-flex mt-3 align-items-center">
          <div className="icon-div bg-blue d-inline-flex mt-2 mr-3">
            <img
              src={`https://myicons.netlify.com/icons/${forecast.WeatherIcon}.svg`}
              alt="icon"
            />
          </div>
          <h2 className="d-inline-block my-0 font-weight-light">
            {forecast.Temperature.Metric.Value} &deg; C
          </h2>
        </span>
      </Fragment>
    );
  } else {
    return null;
  }
};

export default CurrentWeather;
