import React, { Fragment } from "react";
import HeaderVideo from "../../videos/bg-video.mp4";
import Forecast from "./Forecast";
import ForecastResults from "./ForecastResults";
import TopCities from "./TopCities";
import ContactForm from "../contact/ContactForm";

const Home = () => {
  return (
    <Fragment>
      <div className="position-relative div-container">
        <video autoPlay muted loop id="header-video">
          <source src={HeaderVideo} type="video/mp4" />
        </video>
      </div>

      <div className="position-absolute header-container">
        <div className="container">
          <Forecast />
        </div>
      </div>
      <ForecastResults />
      <TopCities />
      <div className="container mt-5">
        <h1 className="section-heading text-light text-center ">Contact Us</h1>
        <div className="row  ">
          <div className="col-md-6 my-4">
            <ContactForm />
          </div>
          <div className="col-md-6 d-flex my-4 align-items-center">
            <ul>
              <li>
                <i className="fa fa-phone mr-2"></i> Phone: +123456789
              </li>
              <li>
                <i className="fa fa-envelope mr-2"></i> Email:
                example@example.com
              </li>
              <li>
                <i className="fa fa-globe mr-2"> </i> www.example.com
              </li>
            </ul>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default Home;
