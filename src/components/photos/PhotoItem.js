import React from "react";
const PhotoItem = ({ PhotoURL, photoGrapher }) => {
  return (
    <div className="col-sm-12 col-md-6 col-lg-4">
      <div className="rounded">
        <img src={PhotoURL} alt="" />
        <h5 className="text-primary mt-2 mb-4">
          <span className="text-light mr-1">Photo Grapher: </span>
          {photoGrapher}
        </h5>
      </div>
    </div>
  );
};

export default PhotoItem;
