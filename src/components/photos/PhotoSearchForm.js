import React, { useContext } from "react";
import { PhotosContext } from "../../contexts/PhotosContext";
const PhotoSearchForm = () => {
  // Getting in the PhotosContext
  const { setPhotos, getImages, query, setQuery } = useContext(PhotosContext);
  // setting query
  const onChangeHandler = (e) => {
    setQuery(e.target.value);
  };
  //
  const onSubmitHandler = (event) => {
    event.preventDefault();
    getImages(query)
      .then((data) => setPhotos(data.photos))
      .catch((err) => console.log(err));
  };
  return (
    <div className="container">
      <div className="row forecast">
        <div className="col-md-12">
          <form onSubmit={onSubmitHandler} className="forecast-form ">
            <div className="input-group bg-dark-light">
              <input
                type="text"
                className="form-control bg-dark-light"
                placeholder="Search Photo"
                id="photos"
                name="photos"
                onChange={onChangeHandler}
              />
              <div className="input-group-append">
                <input
                  type="submit"
                  className="input-group-text btn-forecast-search"
                  id="photoSearch"
                  value="Find"
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default PhotoSearchForm;
