import React, { Fragment, useContext } from "react";
import PhotoItem from "./PhotoItem";
import Breadcrumb from "../utils/Breadcrumb";
import { PhotosContext } from "../../contexts/PhotosContext";
import PhotoSearchForm from "./PhotoSearchForm";
const Photos = (props) => {
  const { photos } = useContext(PhotosContext);
  return (
    <Fragment>
      <Breadcrumb pageName="Photos" />
      <PhotoSearchForm />
      <div className="container">
        <div className="row photos py-4">
          {photos.map((photo) => {
            return (
              <PhotoItem
                PhotoURL={photo.src.landscape}
                photoGrapher={photo.photographer}
                key={photo.id}
              />
            );
          })}
        </div>
      </div>
    </Fragment>
  );
};

export default Photos;
