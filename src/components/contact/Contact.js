import React, { Fragment } from "react";
import ContactForm from "./ContactForm";
import Breadcrumb from "../utils/Breadcrumb";
const Contact = () => {
  return (
    <Fragment>
      <Breadcrumb pageName="Contact us" />
      <div className="contact-form-div">
        <div className="container">
          <div className="row my-5">
            <div className="col-md-6 order-md-1 order-2">
              <div
                className="rounded-me mt-4"
                style={{
                  width: "100%",
                  height: "400px",
                  overflow: "hidden",
                }}
              >
                <iframe
                  title="goglemap"
                  width="100%"
                  height="600"
                  src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=university%20of%20sargodha+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"
                  frameBorder="0"
                  scrolling="no"
                  marginHeight="0"
                  marginWidth="0"
                >
                  <a href="https://www.maps.ie/draw-radius-circle-map/">
                    km radius map
                  </a>
                </iframe>
              </div>
              <br />
            </div>
            <div className="col-md-6 order-md-2 order-1 col-md-offset-1">
              <h2 className="section-title text-light">Contact us</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Commodi consectetur inventore ducimus, facilis, numquam id
                soluta omnis eius recusandae nesciunt vero repellat harum cum.
                Nisi facilis odit hic, ipsum sed!
              </p>
              <ContactForm />
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Contact;
