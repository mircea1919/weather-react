import React from "react";
const ContactForm = () => {
  return (
    <form action="#" className="contact-form">
      <div className="row">
        <div className="col-md-6">
          <input type="text" placeholder="Your name..." />
        </div>
        <div className="col-md-6">
          <input type="text" placeholder="Email Addresss..." />
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <input type="text" placeholder="Company name..." />
        </div>
        <div className="col-md-6">
          <input type="text" placeholder="Website..." />
        </div>
      </div>
      <textarea
        name=""
        placeholder="Message..."
        style={{
          marginTop: " 0px",
          marginBottom: " 0px",
          height: "246px",
        }}
      ></textarea>
      <div className="text-right">
        <input
          className="bg-blue text-light btn rounded-me"
          type="submit"
          placeholder="Send message"
        />
      </div>
    </form>
  );
};

export default ContactForm;
