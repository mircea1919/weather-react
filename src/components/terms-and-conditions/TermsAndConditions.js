import React from "react";
import { Fragment } from "react";
import Breadcrumb from "../utils/Breadcrumb";
const TermsAndConditions = () => {
  return (
    <Fragment>
      <Breadcrumb pageName="Terms" />
      <div className="container">
        <div className="row my-4">
          <div className="col-md-9">
            <div>
              <h1 className="text-light">Terms and Policy</h1>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla
                eaque sapiente ducimus accusantium molestias excepturi harum id
                eligendi, facilis voluptate aut ullam natus. Cumque odit
                molestias atque ullam nulla expedita!
              </p>
            </div>
            <span className="d-block card bg-transparent p-3 text-primary">
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ea,
              inventore quaerat officiis doloremque dignissimos exercitationem
              id quod similique! Maiores
            </span>

            <div className="border-bottom mt-4">
              <h1 className="text-light">How it Works</h1>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla
                eaque sapiente ducimus accusantium molestias excepturi harum id
                eligendi, facilis voluptate aut ullam natus. Cumque odit
                molestias atque ullam nulla expedita!
              </p>
            </div>
            <ul className="py-3">
              <li>It's work with </li>
              <li>It's work with </li>
              <li>It's work with </li>
              <li>It's work with </li>
            </ul>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Distinctio voluptatem facilis ducimus pariatur, vel soluta natus!
              Fugit, corporis laborum eum animi provident, officia, at
              repudiandae tempora temporibus cupiditate nulla nesciunt.lorem
              Lorem ipsum dolor sit amet consectetur adipisicing elit. A nisi
              deleniti excepturi quis, maiores itaque facere nostrum beatae
              autem harum quo officia placeat voluptate ratione odio incidunt
              ipsam labore soluta! Lorem ipsum dolor sit amet consectetur,
              adipisicing elit. Cumque, amet labore facere non maiores aliquid
              qui ipsa ipsum, inventore, autem eveniet nihil officiis quaerat
              explicabo vel neque laboriosam culpa nam.
            </p>
            <a href="#s" className="text-primary">
              Read more
            </a>
          </div>
          <div className="col-md-3">
            <h4 className="text-light my-4">Adds</h4>
            <img src="https://via.placeholder.com/250x200" alt=""></img>
            <span className="d-block my-4"></span>
            <img src="https://via.placeholder.com/250x600" alt=""></img>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default TermsAndConditions;
