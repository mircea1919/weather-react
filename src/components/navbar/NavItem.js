import React from "react";
import { Link } from "react-router-dom";

const NavItem = (props) => {
  return (
    <li className="nav-item active">
      <Link className="nav-link" to={"/" + props.navLink}>
        {props.navItemText}
      </Link>
    </li>
  );
};
export default NavItem;
