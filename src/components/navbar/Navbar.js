import React from "react";
import { Link } from "react-router-dom";
import Logo from "../../img/logo.svg";
import NavItem from "./NavItem";
const Navbar = () => {
  return (
    <nav id="top-nav" className="navbar navbar-expand-md">
      <div className="container">
        <Link className="navbar-brand" to="/">
          <img src={Logo} alt="" />
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <div id="nav-icon1">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </button>
        <div
          className="collapse pul-right navbar-collapse"
          id="navbarSupportedContent"
        >
          <ul className="navbar-nav mr-auto">
            <NavItem navItemText="Home" navLink="" />
            <NavItem navItemText="Photos" navLink="photos" />
            <NavItem navItemText="Blog" navLink="blog" />
            <NavItem navItemText="Contact us" navLink="contact" />
            <NavItem navItemText="Terms and Policy" navLink="terms" />
          </ul>
        </div>
      </div>
    </nav>
  );
};
export default Navbar;
