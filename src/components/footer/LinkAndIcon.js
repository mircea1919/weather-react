import React from "react";
const LinkAndIcon = (props) => {
  return (
    <a href={props.link}>
      <i className={props.icon}></i>
    </a>
  );
};
export default LinkAndIcon;
