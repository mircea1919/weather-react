import React from "react";
import LinkAndIcon from "./LinkAndIcon";

const SocialLinks = () => {
  return (
    <div className="social-links">
      <LinkAndIcon link="#f" icon="fa fa-facebook" />
      <LinkAndIcon link="#f" icon="fa fa-twitter" />
      <LinkAndIcon link="#f" icon="fa fa-google-plus" />
      <LinkAndIcon link="#f" icon="fa fa-pinterest" />
    </div>
  );
};
export default SocialLinks;
