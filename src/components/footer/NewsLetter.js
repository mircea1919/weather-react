import React from "react";
const NewsLetter = () => {
  return (
    <form action="#" className="subscribe-form">
      <input type="text" placeholder="Enter your email and subscribe" />
      <input
        className="bg-blue btn rounded-me text-light"
        type="submit"
        value="Subscribe"
      />
    </form>
  );
};
export default NewsLetter;
