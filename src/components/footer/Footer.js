import React from "react";
import SocialLinks from "./SocialLinks";
import NewsLetter from "./NewsLetter";
import CopyRights from "./CopyRights";
const Footer = () => {
  return (
    <footer className="site-footer">
      <div className="container">
        <div className="row">
          <div className="col-md-8">
            <NewsLetter />
          </div>
          <div className="col-md-3 col-md-offset-1">
            <SocialLinks />
          </div>
        </div>
        <CopyRights text="Qurban Khan All Rights reserved" />
      </div>
    </footer>
  );
};
export default Footer;
