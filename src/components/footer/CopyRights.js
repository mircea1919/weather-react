import React from "react";
const CopyRights = (props) => {
  return <p className="colophon mt-3">{props.text}</p>;
};
export default CopyRights;
