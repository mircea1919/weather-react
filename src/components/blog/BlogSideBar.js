import React from "react";
import SidebarItem from "./SidebarItem";

const BlogSideBar = () => {
  return (
    <div className="col-md-4 my-4">
      <SidebarItem title="New Post" />
    </div>
  );
};
export default BlogSideBar;
