import React from "react";
const Post = ({ post }) => {
  return (
    <div className="blog-post py-4 border-bottom my-2">
      <img
        className="w-100 rounded"
        src="https://via.placeholder.com/700x400"
        alt=""
      />

      <h3 className="blog-title text-light mt-3">{post.title}</h3>
      <p>{post.body}</p>
      <button className="btn rounded-me bg-blue text-light">Read More</button>
    </div>
  );
};
export default Post;
