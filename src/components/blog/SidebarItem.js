import React, { useContext } from "react";
import SidebarItemLink from "./SidebarItemLink";
import { BlogContext } from "../../contexts/BlogContext";
const SidebarItem = (props) => {
  const { posts } = useContext(BlogContext);
  return (
    <ul className="text-light latest-posts rounded">
      <li>
        <h3 className="text-light mb-2">{props.title}</h3>
      </li>
      {posts.map((post) => {
        return <SidebarItemLink icon="fa fa-arrow-right" text={post.title} />;
      })}
    </ul>
  );
};
export default SidebarItem;
