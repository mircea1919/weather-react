import React, { Fragment, useContext } from "react";
import Post from "./Post";
import BlogSideBar from "./BlogSideBar";
// import { BlogContext } from "../../contexts/BlogContext";
import Breadcrumb from "../utils/Breadcrumb";
import { BlogContext } from "../../contexts/BlogContext";
const Blog = () => {
  const { posts } = useContext(BlogContext);

  return (
    <Fragment>
      <Breadcrumb pageName="Blog" />
      <div>
        <div className="container">
          <div className="row">
            <div className="col-md-8">
              {posts.map((post) => {
                return <Post key={post.id} post={post} />;
              })}
            </div>
            <BlogSideBar />
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default Blog;
