import React from "react";
const SidebarItemLink = (props) => {
  return (
    <li>
      <i className={props.icon + " mr-2"}></i> {props.text}
    </li>
  );
};
export default SidebarItemLink;
